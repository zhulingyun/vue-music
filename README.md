# vue-music

> music

## Build Setup

``` bash
# install dependencies
npm install

# serve with hot reload at localhost:8080
npm run dev

# build for production with minification
npm run build

# build for production and view the bundle analyzer report
npm run build --report
```


![推荐页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/recommend.png)
![歌曲列表页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/list.png) 
![播放器页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/player.png) 
![排行页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/rank.png) 
![个人中心页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/center.png) 
![搜索页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/search.png) 
![搜索列表页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/search-list.png)
![歌手页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/singer.png) 
![歌手详情页面](https://gitee.com/zhulingyun/vue-music/raw/master/example_imgs/singer-detail.png) 
 