import {playMode} from 'common/js/config'
import {loadSearch,loadPlay,loadFavorite} from 'common/js/cache'

const state ={
    singer:{},
    playing:false,      //播放状态
    fullScreen:false,   //播放器展开收起
    playlist:[],        //播放列表
    sequenceList:[],    //顺序列表
    mode:playMode.sequence,   //播放模式
    currentIndex:-1,           //当前播放的索引
    
    disc:{},                  //recommend歌单对象
    topList:{},           //推荐歌单
    searchHistory:loadSearch(),
    playHistory:loadPlay(),    //播放历史
    favoriteList:loadFavorite()   //收藏列表
}

export default state;